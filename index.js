var canva = document.getElementById('painter');
var color = document.getElementById('color_picker');
var size = document.getElementById('size_picker');
var text_size = document.getElementById('text_picker');
var eraser = document.getElementById('eraser');
var pencil = document.getElementById('pencil');
var text = document.getElementById('text');
var input = document.getElementById('input');
var font = document.getElementById('font');
var context = canva.getContext('2d');
var mode = "pencil";
var text_font = "serif"
canva.style.cursor = "url('pencil.cur'), auto";

var steps = new Array();
var step = -1;

var pivot = new Array();

var draw = false;

function getMousePos(canva, evt) {
  var rect = canva.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

function mouseMove(evt) {
  var mousePos = getMousePos(canva, evt);
  context.lineTo(mousePos.x, mousePos.y);
  context.stroke();
}

function pushsteps()
{
    step++;
    if (step < steps.length) 
    {
        steps.length = step;
    }
    steps.push(canva.toDataURL());
}

function undo() {
    if (step > 0)
    {
        step--;
        var last = new Image();
        last.src = steps[step];
        last.onload = function()
        {
            context.clearRect(0, 0, canva.width, canva.height);
            context.drawImage(last,0,0);
        }
    }
    else if(step == 0)
    {
            step--;
            context.clearRect(0, 0, canva.width, canva.height);
    }
}      

function redo() {
    if (step < steps.length-1) 
    {
        step++;
        var next = new Image();
        next.src = steps[step];
        next.onload = function()
        {
            context.clearRect(0, 0, canva.width, canva.height);
            context.drawImage(next,0,0);
        }
        
    }
}                        

canva.addEventListener('mousedown', function(evt) {
  var mousePos = getMousePos(canva, evt);
  context.beginPath();
  context.moveTo(mousePos.x, mousePos.y);
  evt.preventDefault();
  draw = true;
  if(mode == "rectangle" || mode == "circle")
  {
    if(pivot.length<2)
        pivot.push({x:mousePos.x, y:mousePos.y});
  }
  else if(mode == "triangle")
  {
      if(pivot.length<3)
        pivot.push({x:mousePos.x, y:mousePos.y});
  }
});

canva.addEventListener('mousemove',function(evt){
    if(draw)
    {   
        var mousePos = getMousePos(canva, evt);
        switch(mode)
        {
            case "pencil":
                context.lineTo(mousePos.x,mousePos.y);
                context.stroke();
                break;
            case "eraser":
                context.clearRect(mousePos.x, mousePos.y, 10, 10);
                break;
        }
    }
});

canva.addEventListener('mouseup', function(evt) {
    var mousePos = getMousePos(canva, evt);
    if(mode == "text")
    {
        context.font = text_size.value + 'px ' + text_font;
        context.fillText(input.value, mousePos.x, mousePos.y);
    }
    else if(mode == "rectangle")
    {
        if(pivot.length==2)
        {
            context.fillRect(pivot[0].x,pivot[0].y,pivot[1].x-pivot[0].x,pivot[1].y-pivot[0].y);
            while(pivot.length>0)
                pivot.pop();
        }
    }
    else if(mode == "circle")
    {
        if(pivot.length==2)
        {
            context.arc(pivot[0].x,pivot[0].y,Math.sqrt((pivot[1].x-pivot[0].x)*(pivot[1].x-pivot[0].x)+(pivot[1].y-pivot[0].y)*(pivot[1].y-pivot[0].y)),0,Math.PI*2,true);
            while(pivot.length>0)
                pivot.pop();
        }
    }
    else if(mode == "triangle")
    {
        if(pivot.length==3)
        {
            context.moveTo(pivot[0].x,pivot[0].y);
            context.lineTo(pivot[1].x,pivot[1].y);
            context.lineTo(pivot[2].x,pivot[2].y);
            context.fill();
        }
        while(pivot.length>0)
            pivot.pop();
    }
    draw = false;
    if(mode == "circle"||mode == "triangle")
        {
            context.fill();
        }
    context.closePath();
    pushsteps();
});

canva.addEventListener('mouseleave',function(){
    draw = false;
    context.closePath();
});

color.addEventListener('input', function()
{
    context.strokeStyle = color.value;
});

size.addEventListener('input', function()
{
    context.lineWidth = size.value;
});


document.getElementById('reset').addEventListener('click', function() {
  context.clearRect(0, 0, canva.width, canva.height);
  context.strokeStyle = color.value;
  draw = false;
  mode = "pencil";
  canva.style.cursor = "url('pencil.cur'), auto";
  while(steps.length>0)
    steps.pop();
  while(pivot.length>0)
    pivot.pop();
  step = -1;
});

eraser.addEventListener('click', function() {
    mode = "eraser";
    canva.style.cursor = "url('eraser.cur'), auto";
});

pencil.addEventListener('click', function() {
    mode = "pencil";
    canva.style.cursor = "url('pencil.cur'), auto";
});

text.addEventListener('click', function() {
    mode = "text";
    canva.style.cursor = "text";
});
  
document.getElementById('serif').addEventListener('click', function(){
    text_font = "serif";
});

document.getElementById('sans-serif').addEventListener('click', function(){
    text_font = "sans-serif";
});

document.getElementById('fantasy').addEventListener('click', function(){
    text_font = "fantasy";
});

document.getElementById('undo').addEventListener('click', function()
{
    undo();
    console.log("undo:",step);
    //console.log("undo:",steps[step]);
});

document.getElementById('redo').addEventListener('click', function()
{
    redo();
    console.log("redo:",step);
    //console.log("redo:",steps[step]);
});

document.getElementById('download').addEventListener('click', function(){
    var image = canva.toDataURL("image/png");
    document.getElementById('download').href = image;
});

document.getElementById('circle').addEventListener('click', function()
{
    mode = "circle";
    canva.style.cursor = "crosshair";
});

document.getElementById('rectangle').addEventListener('click', function()
{
    mode = "rectangle";
    canva.style.cursor = "crosshair";
});

document.getElementById('triangle').addEventListener('click', function()
{
    mode = "triangle";
    canva.style.cursor = "crosshair";
});